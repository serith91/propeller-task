$(document).ready(function() {
  var menuAbout = $(".about");
  var menuMenus = $(".menus");
  var menuLocations = $(".locations");
  var menuBookings = $(".bookings");
  var menuGallery = $(".gallery");
  var menuContact = $(".contact");

  var headerFacebook = $(".facebook");
  var headerTwitter = $(".twitter");
  var headerInstagram = $(".instagram");

  var footerFacebook = $(".--facebook");
  var footerTwitter = $(".--twitter");
  var footerInstagram = $(".--instagram");

  var bookOverlay = $(".book_overlay");

  var partitionBtn = $(".partition_button");

  // User Interface Functionality
  menuAbout.click(function() {
    // Functionality Goes Here.
  });

  menuMenus.click(function() {
    // Functionality Goes Here.
  });

  menuLocations.click(function() {
    // Functionality Goes Here.
  });

  menuBookings.click(function() {
    // Functionality Goes Here.
  });

  menuGallery.click(function() {
    // Functionality Goes Here.
  });

  menuContact.click(function() {
    // Functionality Goes Here.
  });

  headerFacebook.click(function() {
    // Functionality Goes Here.
  });

  headerTwitter.click(function() {
    // Functionality Goes Here.
  });

  headerInstagram.click(function() {
    // Functionality Goes Here.
  });

  footerFacebook.click(function() {
    // Functionality Goes Here.
  });

  footerTwitter.click(function() {
    // Functionality Goes Here.
  });

  footerInstagram.click(function() {
    // Functionality Goes Here.
  });

  bookOverlay.click(function() {
    // Functionality Goes Here
  });

  partitionBtn.click(function() {
    // Functionality Goes Here
  });
});
