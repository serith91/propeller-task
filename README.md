Propeller Task - By Thomas Beton
-------------

## Install:
* Clone the repository from BitBucket.
* Navigate with **Command Prompt** to the directory.
* Use '**npm install**' to install the dependencies.
* Use '**npm install -g http-server**' to install the http-server.
* Use '**npm run start**' to start the http-server.
* Go to '**localhost:8080**' in your Chrome browser.

## Features:
* Responsive layout down to 420px
* Responsive on iPhone 6 Plus chrome dev tools, portrait.
* jQuery set up.

## screenshot album:
* http://imgur.com/a/jR0N2
